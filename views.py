from app import app
from flask import render_template, redirect, url_for
from models import *


@app.route("//", methods = ("GET","POST"))
def login():
    """ Renvoie une vue pour se connecter
    """
    return render_template(
        "login.html"
    )
    
@app.route("/register/", methods = ("GET",))
def register():
    """ Renvoie une vue pour s'inscrire
    """
    return render_template(
        "register.html"
    )

@app.route("/<pseudoUser>/playlist/vue/<id>/delete/<idAlbum>", methods = ("GET", "POST",))
def delete_album_playlist(idAlbum,pseudoUser,id):
    play = get_playlist_by_id(id)
    album = get_album_by_id(idAlbum)
    play.album.remove(album)
    db.session.commit()
    return redirect(url_for('playlist',pseudoUser=pseudoUser,id=id))

@app.route("/<pseudoUser>/createalbum/", methods = ("GET", "POST",))
def createalbum(pseudoUser):
    return render_template("createalbum.html",pseudoUser=pseudoUser)


@app.route("/<pseudoUser>/createalbum/added", methods = ("GET", "POST",))
def add_album_bd(pseudoUser):
    titre= request.form.get("Titre")
    dateDeSortie=request.form.get("Annee")
    urlImage=request.form.get("Annee")
    artiste=request.form.get("Artiste")

    auteur_id= get_id_by_artiste(str(artiste))
    i=auteur_id
    print(auteur_id)
    if i==[]:
        auteur_id=get_max_new_id()
        art= Artist(id_artist=auteur_id,nomPrenom=artiste)
        db.session.add(art)
        db.session.commit()
    add_album(titre,dateDeSortie,urlImage,auteur_id)
    return redirect(url_for('accueil',pseudoUser=pseudoUser))

def add_album(titre,dateDeSortie,urlImage,auteur_id):
    album = Album(id=get_max_new_id(),
    auteur_id=auteur_id,
    titre=titre,
    dateDeSortie=dateDeSortie,
    urlImage=urlImage,
    )
    db.session.add(album)
    db.session.commit()



@app.route("/<pseudoUser>/search/", methods = ("GET","POST",))
def maj_search(pseudoUser):
    genre=get_all_genres()

    recherche=str(request.form.get("search"))
    recherche2=request.form.get('myhtml')
    name = request.form.get('my_html_select_box')

   
    if recherche!=None and name=="Album" and recherche2=="Aucun":
        album = Album.query.filter(Album.titre.like(recherche+'%'))
        return render_template("rechercher.html",album=album,funcArtist=get_artist_by_id,genre=genre,pseudoUser=pseudoUser)
    
    elif recherche!=None and name=="Artiste" and recherche2=="Aucun":
        artiste = Artist.query.filter(Artist.nomPrenom.like(recherche+'%')).all() 
        print(artiste)
        res2 =[]
        for elem in artiste:
            print(elem)
            res2.append(elem.id_artist)
        album = Album.query.filter(Album.auteur_id.in_(res2)).all()
        return render_template("rechercher.html",album=album,funcArtist=get_artist_by_id,genre=genre,pseudoUser=pseudoUser)

    elif recherche!=None and name=="Genre" and recherche2=="Aucun":
        genres = Genre.query.filter(Genre.nom.like(recherche+'%')).first()
        print(genres)
        album = genres.albums
        return render_template("rechercher.html",album=album,funcArtist=get_artist_by_id,genre=genre,pseudoUser=pseudoUser)
    
    elif recherche==None and name=="Genre" and recherche2!="Aucun":
        genres = Genre.query.filter(Genre.nom.like(recherche2)).first()
        print(genres)
        album = genres.albums
        return render_template("rechercher.html",album=album,funcArtist=get_artist_by_id,genre=genre,pseudoUser=pseudoUser)
    
    elif recherche2!="Aucun":
        genres = Genre.query.filter(Genre.nom.like(recherche2)).first()
        album = genres.albums
        return render_template("rechercher.html",album=album,funcArtist=get_artist_by_id,genre=genre,pseudoUser=pseudoUser)

    return render_template("rechercher.html",genre=genre,pseudoUser=pseudoUser)




@app.route("/<pseudoUser>/rechercher/", methods = ("GET","POST"))
def rechercher(pseudoUser):
    albums = Album.query.order_by(Album.dateDeSortie.desc()).all()
    return render_template("rechercher.html", pseudoUser = pseudoUser, albums = albums, funcArtist=get_artist_by_id)


@app.route("/<pseudoUser>/accueil/", methods = ("POST","GET"))
def accueil(pseudoUser):
    """ Renvoie une vue pour s'inscrire
    """
    albums = Album.query.order_by(Album.dateDeSortie.desc()).all()
    artistes = Artist.query.order_by(func.random()).limit(12).all()
    a = get_artist_by_id
    return render_template(
        "accueil.html",
        artistes=artistes,
        albums=albums,
        funcArtist=a,
        pseudoUser = pseudoUser
    )

@app.route("/<pseudoUser>/accueil/2", methods = ("POST","GET"))
def accueil2(pseudoUser):
    albums = Album.query.order_by(Album.dateDeSortie.desc()).all()
    artistes = Artist.query.order_by(func.random()).limit(12).all()
    a = get_artist_by_id
    return render_template("accueil2.html",artistes=artistes,
        albums=albums,
        funcArtist=a
        ,pseudoUser=pseudoUser
    )

@app.route("/<pseudoUser>/accueil/3", methods = ("POST","GET"))
def accueil3(pseudoUser):
    albums = Album.query.order_by(Album.dateDeSortie.desc()).all()
    artistes = Artist.query.order_by(func.random()).limit(12).all()
    a = get_artist_by_id
    return render_template("accueil3.html",artistes=artistes,
        albums=albums,
        funcArtist=a
        ,pseudoUser=pseudoUser
    )
@app.route("/<pseudoUser>/accueil/4", methods = ("POST","GET"))
def accueil4(pseudoUser):
    albums = Album.query.order_by(Album.dateDeSortie.desc()).all()
    artistes = Artist.query.order_by(func.random()).limit(12).all()
    a = get_artist_by_id
    return render_template("accueil4.html",artistes=artistes,
        albums=albums,
        funcArtist=a,
        pseudoUser=pseudoUser
    )

@app.route("/accueil/regi", methods = ("GET", "POST",))
def register_home():
    users_names = get_pseudos_users()
    users_mails = get_mail_users()
    pseudo = request.form.get('pseudo')
    mail = request.form.get('email')
    nom = request.form.get('nom')
    prenom = request.form.get('prenom')
    mdp = request.form.get('mdp')
    sexe = request.form.get('sexe')
    dateNaissance = request.form.get('birth')
    print(users_names)
    print(users_mails)
    print("mails")
    for elem in users_names:
        if str(pseudo) == str(elem) :
            return redirect(url_for('pb_pseudo'))
    for m in users_mails:
        if str(m) == str(mail):
            return redirect(url_for('pb_mail'))
    add_user(pseudo,mail,nom,prenom,mdp,sexe,dateNaissance)
    return render_template("registerOK.html")
    


@app.route("/accueil/probleme", methods = ("POST",))
def login_home():
    pseudo = request.form.get('pseudo')
    mdp = request.form.get('mdp')
    test = get_pseudo_mdp(pseudo,mdp)
    if test :
        User = recuperer_pseudo(pseudo)
        return redirect(url_for('accueil',pseudoUser=pseudo))
    else:
        return render_template("problemeConnexion.html")

@app.route("/<pseudoUser>/playlist/Ajouter", methods = ("GET", "POST",))
def playlistAjouter(pseudoUser):
    """ Renvoie une vue pour s'inscrire
    """
    nom = request.form.get('nom')
    add_playlist(nom)
    return redirect(url_for('playlist',pseudoUser=pseudoUser))


@app.route("/<pseudoUser>/playlist/", methods = ("GET", "POST",))
def playlist(pseudoUser):
    """ Renvoie une vue pour s'inscrire
    """
    playlists = get_playlist()
    return render_template(
        "playlist.html"
        ,pseudoUser=pseudoUser,
        playlist=playlists)
   


@app.route("/<pseudoUser>/playlist/vue/<id>/", methods = ("GET", "POST",))
def vue_playlist(id,pseudoUser):
    playlist = get_playlist_by_id(id)
    print(playlist)
    funcArtist = get_artist_by_id
    return render_template("vue_playlist.html",playlist=playlist,funcArtist=funcArtist,pseudoUser=pseudoUser)

@app.route("/<pseudoUser>/playlist/<id>/delete/", methods = ("GET", "POST",))
def delete_playlist(id,pseudoUser):
    play = get_playlist_by_id(id)
    if play != []:
        db.session.delete(play)
        db.session.commit()
    return redirect(url_for('playlist',pseudoUser=pseudoUser))



@app.route("/<pseudoUser>/profil/", methods = ("GET", "POST",))
def profil(pseudoUser):
    """ Renvoie une vue pour s'inscrire
    """
    user = recuperer_pseudo(pseudoUser)
    return render_template(
        "profil.html"
        ,pseudoUser=pseudoUser,
        user = user
    )

@app.route("/register/pseudo", methods = ("GET", "POST",))
def pb_pseudo():
    return render_template(
        "problemePseudo.html"
    )

@app.route("/register/mail", methods = ("GET", "POST",))
def pb_mail():
    return render_template(
        "problemeMail.html"
        
    )

@app.route("/<pseudoUser>/accueil/info/<id>/", methods = ("POST", "GET",))
def info_album(id,pseudoUser):
    album1 = get_album_by_id(id)
    playlist = get_playlist()
    play = request.form.get("playlists")
    for elem in playlist:
        if play == elem.nom:
            p=get_playlist_by_name(play)
            print("jsuis la ")
            # print(album1)
            add_album_in_playlist(p,album1)
    print(play)
    return render_template(
        "infoAlbum.html",
        album = album1,
        playlist=playlist
        ,pseudoUser=pseudoUser
    )

@app.route("/<pseudoUser>/accueil/info/<id>/modification", methods = ("GET", "POST",))
def modif_album(id,pseudoUser):
    album1 = get_album_by_id(id)
    playlist = get_playlist()
    play = request.form.get("my_html_select_box")
    print(play)
    return render_template(
        "modifAlbum.html",
        album = album1,
        playlist=playlist,
        pseudoUser=pseudoUser
    )

# @app.route("/playlist", methods = ("POST","GET",))
# def playlistAjouter():
#     nom = request.form.get()
#     add_playlist(nom)
#     return  redirect(url_for("playlist"))
@app.route("/<pseudoUser>/accueil/<idArtist>/editArtist", methods = ("GET", "POST",))
def editArtist(pseudoUser,idArtist):
    return render_template("editArtist.html",pseudoUser=pseudoUser,idArtist=idArtist)


@app.route("/<pseudoUser>/accueil/<idArtist>/editArtist/edited", methods = ("GET", "POST",))
def modifArtist(pseudoUser,idArtist):
    nom = request.form.get('nom')
    print(idArtist)
    print(nom)
    if nom != '':
        a = get_artist_by_id(idArtist)
        a.nomPrenom = nom
        db.session.commit()
    return redirect(url_for('accueil',pseudoUser=pseudoUser))

@app.route("/<pseudoUser>/accueil/<idArtist>/editArtist/deleted", methods = ("GET", "POST",))
def deleteArtist(pseudoUser,idArtist):
    a = get_artist_by_id(idArtist)
    if a != []:
        db.session.delete(a)
        db.session.commit()
    return redirect(url_for('accueil',pseudoUser=pseudoUser))


@app.route("/<pseudoUser>/accueil/info/<id>/edited", methods = ("GET", "POST",))
def edit_album(id,pseudoUser):
    a = get_album_by_id(id)
    
    titre = request.form.get('Titre')
    annee = request.form.get('Annee')
    url = request.form.get('URL')
    genres = request.form.get('Genre')
    alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    if genres != "":
        for g in get_all_genres():
            if g in a.genres:
                a.genres.remove(g)
        mot=""
        for elem in genres:
            if elem in alphabet:
                mot+=elem
                print("if1")
                print(mot)
            else:
                if mot!="":
                    print("if3")
                    print(mot)
                    if (mot in get_all_genres()):
                        print("if4")
                        gBD = get_genre_by_nom(mot)
                        a.genres.append(gBD)
                        mot=""
                    else:
                        print("if4")
                        a.genres.append(Genre(id=get_max_new_id_genre(),nom=mot,))
                        mot=""
        if mot!="":
                    print("if3")
                    print(mot)
                    if (mot in get_all_genres()):
                        print("if4")
                        gBD = get_genre_by_nom(mot)
                        a.genres.append(gBD)
                        mot=""
                    else:
                        print("if4")
                        a.genres.append(Genre(id=get_max_new_id_genre(),nom=mot,))
                        mot=""

    if titre != "":
        a.titre = titre
    if annee != "":
        a.dateDeSortie = annee
    if url !="":
        a.urlImage = url
    db.session.commit()
    album=get_album_by_id(id)

    return render_template(
        "infoAlbum.html",
        id=id,
        pseudoUser = pseudoUser,
        album = album,
    )

@app.route("/<pseudoUser>/jedelete/<id>", methods = ("GET", "POST",))
def delete_album(id,pseudoUser):
    a = get_album_by_id(id)
    if a != []:
        db.session.delete(a)
        db.session.commit()
    return redirect(url_for('accueil',pseudoUser=pseudoUser))
 


def add_user(pseudo1,mail1,nom1,prenom1,mdp1,sexe1,birth1):
    user = Utilisateur(id = get_max_new_id(),
    pseudo = pseudo1,
    mail = mail1,
    nom = nom1,
    prenom = prenom1,
    mdp = mdp1,
    sexe = sexe1,
    dateNaissance = birth1)
    db.session.add(user)
    db.session.commit()

def add_playlist(nom):
    id=get_max_new_id_playlist()
    names = get_playlist_name(nom)
    print(names)
    if names== True:
        return print("changer de nom")
    else:
        play = Playlist(
        id=id,
        nom=nom )
        
        db.session.add(play)
        db.session.commit()

def add_album_in_playlist(playlist1,albums):
    playlist1.album.append(albums)



    # p= albums.playlist
    # print(playlist1)
    # print(p.album)
    print("weshh")
    # print(albums.playlist)
    # p.append(playlist1)
    db.session.commit()
    

if __name__ == '__main__':
    app.run(debug=False)

