from asyncio.windows_events import NULL
import datetime
from app import app , db
from models import Artist, Album,Genre,association_genre_album, get_genre_by_nom

@app.cli.command()
def loaddb():
    """
    Création d'une nouvelle base de données après supression de celle existante.
    """
    db.drop_all()
    db.create_all()

@app.cli.command()
def syncdb():
    """
    Création des tables de la base de données
    """

    # chargement de notre jeu de données
    import yaml
    with open("data/extrait.yml") as file:
        data = yaml.load(file, Loader=yaml.FullLoader)
    albums = Album.query.filter(Album.id).all()
    genres = Genre.query.filter(Genre.id).all()
    
    # création des artistes
    artists = {}
    cpt = 0
    for a in data:
        if a["by"] not in artists:
            artist = Artist(id_artist=cpt, nomPrenom=a["by"])
            db.session.add(artist)
            artists[a["by"]] = artist
            cpt+=1
    db.session.commit()
    
    cpt=0
    liste_genre = []
    for a in data:
        art = artists[a["by"]]
        release = int(a["releaseYear"])
        if release != 0:
            date = datetime.date(year=release,month=1,day=1)
            new_album = Album(id=a["entryId"],auteur_id=art.id_artist,titre=a["title"],dateDeSortie=date,urlImage=a["img"])
        else:
            new_album = Album(id=a["entryId"],auteur_id=art.id_artist,titre=a["title"],dateDeSortie=None,urlImage=a["img"])
        for elem in a["genre"]:
            if elem not in liste_genre:
                new_genre=Genre(id=cpt,nom=elem)
                db.session.add(new_genre)
                alb = new_genre.albums
                alb.append(new_album)
                liste_genre.append(elem)
                cpt+=1
            else:
                g = get_genre_by_nom(elem)
                alb = g.albums
                alb.append(new_album)
        if a["genre"] == []:
                db.session.add(new_album)


    db.session.commit()        
